/*
 * File: statistics.component.ts
 * Project: comps
 * File Created: Thursday, 30th March 2023 11:55:37 am
 * Last Modified: Thursday, 30th March 2023 12:32:17 pm
 * Copyright 2023 Vitor Leal
 */

import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css'],
})
export class StatisticsComponent {
  @Input() data: any;
}
