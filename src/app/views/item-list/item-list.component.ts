/*
 * File: item-list.component.ts
 * Project: comps
 * File Created: Thursday, 30th March 2023 12:10:05 pm
 * Last Modified: Thursday, 30th March 2023 12:31:55 pm
 * Copyright 2023 Vitor Leal
 */

import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.css'],
})
export class ItemListComponent {
  @Input() items: any;
}
