/*
 * File: views-routing.module.ts
 * Project: comps
 * File Created: Thursday, 30th March 2023 11:52:01 am
 * Last Modified: Thursday, 30th March 2023 12:32:47 pm
 * Copyright 2023 Vitor Leal
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ViewsHomeComponent } from './views-home/views-home.component';

const routes: Routes = [
  {
    path: '',
    component: ViewsHomeComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewsRoutingModule {}
