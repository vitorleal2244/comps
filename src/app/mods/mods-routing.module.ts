/*
 * File: mods-routing.module.ts
 * Project: comps
 * File Created: Thursday, 30th March 2023 10:05:45 am
 * Last Modified: Thursday, 30th March 2023 12:34:27 pm
 * Copyright 2023 Vitor Leal
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ModsHomeComponent } from './mods-home/mods-home.component';

const routes: Routes = [
  {
    path: '',
    component: ModsHomeComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModsRoutingModule {}
