/*
 * File: mods.module.ts
 * Project: comps
 * File Created: Thursday, 30th March 2023 10:05:45 am
 * Last Modified: Thursday, 30th March 2023 12:35:40 pm
 * Copyright 2023 Vitor Leal
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModsRoutingModule } from './mods-routing.module';
import { SharedModule } from '../shared/shared.module';
import { ModsHomeComponent } from './mods-home/mods-home.component';
import { ModalComponent } from './modal/modal.component';
import { AccordionComponent } from './accordion/accordion.component';

@NgModule({
  declarations: [ModsHomeComponent, ModalComponent, AccordionComponent],
  imports: [CommonModule, ModsRoutingModule, SharedModule],
})
export class ModsModule {}
